from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Status
from .forms import StatusForm

# Create your views here.

def index(request):
    form = StatusForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            inputName = form.data['userName']
            inputStatus = form.data['userStatus']
            request.session['inputName'] = inputName
            request.session['inputStatus'] = inputStatus        
            return redirect('/confirm')
    else:
        data = Status.objects.all()
        response = {'Data':data, 'status': form}
        return render(request, 'index.html', response)


def confirm(request):
    inputName = request.session['inputName']
    inputStatus = request.session['inputStatus']
    if request.method == 'POST':
        if "cancel" in request.POST:
            return redirect('/')
        else:
            Status.objects.create(userName = inputName, userStatus = inputStatus)
            return redirect('/')
    else:
        context = {
            'nameName': inputName,
            'statusStatus': inputStatus,
        }
        return render(request, 'confirm.html', context)
